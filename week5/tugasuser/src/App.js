import React from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import { Navbar, Form, FormControl, Button, Nav } from 'react-bootstrap';
import Home from '../src/syohan/Home';
import Swiss from '../src/syohan/Swiss';
import Jerman from '../src/syohan/Jerman';
import Turkey from '../src/syohan/Turkey';
import './syohan/style.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const App = () => {
  return (
    <div>
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand href='/home'>Navbar</Navbar.Brand>
        <Nav className='mr-auto'>
          <Link to='/home' className='mr-sm-4'>Home</Link><br />
          <Link to='/turkey' className='mr-sm-4'>Turkey</Link><br />
          <Link to='/jerman' className='mr-sm-4'>Jerman</Link><br />
          <Link to='/swiss' className='mr-sm-4'>Swiss</Link><br />
        </Nav>
        <Form inline>
          <FormControl type='text' placeholder='search' className='mr-sm-2' />
          <Button variant='outline-info'>Search</Button>
        </Form>
      </Navbar>

      
      <Switch>
        <Route path='/turkey'>
          <Turkey />
        </Route>
        <Route path='/jerman'>
          <Jerman />
        </Route>
        <Route path='/swiss'>
          <Swiss />
        </Route>
        <Route path='/'>
          <Home />
        </Route>
      </Switch>
      

    </div>
  )
}

export default App;