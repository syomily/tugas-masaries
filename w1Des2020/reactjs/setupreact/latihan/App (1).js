import logo from './logo.svg';
import './App.css';
import React from 'react';
import Task1 from "./component/task1";
import Task2 from "./component/task2";

function App() {
  return (
    <>
    <Task1/>
    <Task2/>

    </>
  );
}

export default App;
