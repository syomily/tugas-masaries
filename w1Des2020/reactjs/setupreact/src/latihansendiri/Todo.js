import React, {useState} from 'react'
import TodoForm from './TodoForm'
import { RiCloseCircleLine } from 'react-icons/ri'
import { TiEdit } from 'react-icons/ti'

function ToDo(todos,completeTodo, removetodo, updateTodo) {
    const [edit, setEdit] = useState({
        id : Null,
        value: ""
    })

    const submitUpdate = value => {
        updateTodo(edit.id, value)
        setEdit({
            id : Null,
            value: ' '
        })
    }

    if(edit.id) {
        return <TodoForm edit={edit} onSubmit={submitUpdate} />;
    }
    

    return todos.map((todo, index) => (
        <div className={todo.isComplete ? "todo-row complete" : "todo-row"} key=
        {index}
    >
        <div key={todo.id}  onClick={() => completeTodo(todo.id)}>
            {todo.text}
        </div>
        <div clasName="icons">
            <RiCloseCircleLine 
            onClick={() => removetodo(todo.id)}
            className='delete-icon'
            /> 
            <TiEdit onClick={() => setEdit({ id: todo.id, value: todo.text})}
            className='edit-icon'/>
        </div>
        </div>
    ))
}

export default ToDo
