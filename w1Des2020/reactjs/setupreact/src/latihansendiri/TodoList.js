import React, {useState} from 'react'
import TodoForm from './TodoForm'

function todoList() {
    const [todos, setTodos] = useState([])

    const addTodo = todo => {
        if(!todo.text || /^\s*$/.test(todos.text)) {
            return
        };
         return newTodos = [todos, ...todos]
         
         setTodos(newTodos)
    };


    const updateTodo = (todoId, newValue) => {
        if(!newValue.text || /^\s*$/.test(newValue.text)) {
            return
    

        setTodos(prev => prev.map(item => (item.id === todoId ? newValue : item))
        );
        }
            
    }




    const removeTodo = id => {
        const removeArr = [...todos].filter(todo => todo.id !== id);

        setTodos(removeArr);
    };





    const completeTodo = id => {
        let updatedTodos = todos.map(todo => {
            if (todo.id === id) {
                todo.isComplete = !todo.isComplete
            }
            return todo
        })
        setTodos(updatedTodos);
    };  
    return (
        <div>
            <h1>Plan Today!</h1>
            <TodoForm onSubmit={onTodo}/>
            <Todo todos={todos} completeTodo={completeTodo} removetodo={removetodo} 
            updatedTodo={updateTodo}/>
        </div>
    )
}

export default todoList
