// // import SuperCentered from "../src/components/SuperCentered";
// // import Pancake from "../src/components/pancake";
// // import Say from "../src/components/sidebar"
// // import Pancakestake from "../src/components/pancakestake"
// // import Holygrail from "../src/components/holygrail"
// // import SpanGrid from "../src/components/spanGrid"
// // import RAM from "../src/components/RAM"
// // import Lineitup from "../src/components/lineitup"
// // import Clamping from "../src/components/clamping"
// // import Respect from "../src/components/respect"
// // import Navbar from "../src/jsxtask/navbar"
// import React from "react";
// import task1 from './components/task1copy'
// import "./App.css";
// import TodoList from "./w1tugas/TodoList";
// import tugasweekend from './components/tugasweekend';

/*------------------- TUGAS AWAL -------------------*/
// // function App() {
// //   return (
// //     <div className="App">
// //       <SuperCentered/>
// //       <Pancake/>
// //       <Say/>
// //       <Pancakestake/>
// //       <Holygrail/>
// //       <SpanGrid/>
// //       <RAM/>
// //       <Lineitup/>
// //       <Clamping/>
// //       <Respect/>
// //       <Navbar/>

// //     </div>
// //   );

// // }

// // export default App;


/*------------- TUGAS TODOLIST ------------------*?
// function App () {
//   return (
//     <div className ="to do-app">
//       <TodoList />
//     </div>
//   );
// }
 
// export default App;


/*---------- TUGAS TASK1 dan TASK2 --------------*/
// import './App.css';
// import React from 'react';
// import Task2 from './components/task2copy';
// // import Task1 from './components/task1copy';

// function App() {
//   return (
//     <>
//     {/* <Task1/> */}
//     <Task2/>

//     </>
//   );
// }

// export default App;


// function Welcome () {
//   return (
//     <div className ="isLoggedIn">
//       <Welcome />
//     </div>
//   );
// }
 
// export default Welcome;




/*------TUGAS BIO + BLOG + ARTIKEL ---------*/
// import './App.css';
// import React from 'react';
// import {BrowserRouter, Route, Switch} from "react-router-dom";
// import Bio from './components/syohanbio';
// import Blog from './components/syohanblog';
// import Artikel from './components/artikel';


// function App() {
//   return (
//     <BrowserRouter>
//       <Bio/>
//       <Switch>
//         <Route exact path="/" component={Bio}/>
//         <Route exact path="/blog" component={Blog}/>
//         <Route exact path="/artikel" component={Artikel}/>
//       </Switch>
//     </BrowserRouter>
    
    

    
//   );
// }

// export default App;


/*------------- TUGAS USER ----------------------------*/
import React from 'react';
import { Switch, Route, BrowserRouter, Link } from 'react-router-dom';
import { Navbar, Form, FormControl, Button, Nav } from 'react-bootstrap';
import Home from './tugasuser/Home';
import Swiss from './tugasuser/Swiss';
import Jerman from '../src/tugasuser/Jerman';
import Turkey from './tugasuser/Turkey';
import Axios from './tugasuser/Axios';
import Fetch from './tugasuser/fetch';
import './tugasuser/style.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const App = () => {
  return (
    <BrowserRouter>
    <div>
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand href='/home'>Navbar</Navbar.Brand>
        <Nav className='mr-auto'>
          <Link to='/home'>Home</Link>
          <br></br>
          <Link to='/turkey'>Turkey</Link>
          <br></br>
          <Link to='/jerman'>Jerman</Link>
          <br></br>
          <Link to='/swiss'>Swiss</Link>
          <br></br>
          <Link to='/axios'>Axios</Link>
          <br></br>
          <Link to='/fetch'>Fetch</Link>
        </Nav>
        <Form inline>
          <FormControl type='text' placeholder='search' className='mr-sm-2' />
          <Button variant='outline-info'>Search</Button>
        </Form>
      </Navbar>

  
      <Switch>
        <Route path='/turkey'>
          <Turkey />
        </Route>
        <Route path='/jerman'>
          <Jerman />
        </Route>
        <Route path='/swiss'>
          <Swiss />
        </Route>
        <Route path='/axios'>
          <Axios />
          </Route>
        <Route path='/fetch'>
          <Fetch />  
        </Route>
        <Route path='/'>
          <Home />
        </Route> 
        <Route exact path='/' component={Home} />
      </Switch>
      
     
      
  
    </div>
    </BrowserRouter>
  )
}

export default App;