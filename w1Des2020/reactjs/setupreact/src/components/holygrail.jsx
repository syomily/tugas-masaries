import Style from "../assest/holygrail.module.css"
import React from "react";

class Holygrail extends React.Component {
    render() { 
        return (
        <div className ={Style.body}>
            <div className ={Style.header}>Header.com</div>
            <div className ={Style.leftSidebar}>left Sidebar</div>
            <div className ={Style.main}></div>
            <div className ={Style.rightSidebar}>"right Sidebar"</div>
            <div className ={Style.footer}></div>
        </div>
        )
    }   
}

export default Holygrail;