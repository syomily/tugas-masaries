import Style from "../assest/pancakestake.module.css";
import React from "react";

class Pancakestake extends React.Component {
    render() { 
        return (
        <div className ={Style.body}>
            <div className ={Style.header}>Header.com</div>
            <div className ={Style.main}></div>
            <div className ={Style.footer}>Footer Content — Header.com 2020</div>
        </div>
        )
    }   
}

export default Pancakestake;