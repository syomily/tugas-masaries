import styles from "../assest/lineitup.module.css";
import React from "react";

class lineitup extends React.Component {
    render() { 
        return (
        <div className ={styles.body}>
            <div className={styles.card}>
            <h1 className={styles.h1}>Title - Card 1</h1>
            <p> Medium length description. Let's add a few more words here.</p>
            <div className={styles.visual}></div>
            </div>
            <div className={styles.card}>
            <h1 className={styles.h1}>Title - Card 2</h1>
            <p> Long Description. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sed est error repellat veritatis.</p>
            <div className={styles.visual}></div>
            </div>
            <div className={styles.card}>
            <h1 className={styles.h1}>Title - Card 3</h1>
            <p>Short Description.</p>
            <div className={styles.visual}></div>
            </div>
        </div>
        )
    }   
}

export default lineitup;