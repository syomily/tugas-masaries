import React, {useState, useEffect} from 'react';
import styles from "../assest/task2copy.module.css";
import {FormGroup, Label, Input} from 'reactstrap';
// import {dataJson} from "./Json"



const Phase1 = () => {

  const [todo, setTodo] = useState("") //pengganti setState
  const [arr, setArr] = useState([]) //pengganti setState
  const [index, setIndex] = useState(0) //pengganti setState

  const dataJson = [
    {
      name: "Memasak",
      status: true,
    },
    {
      name: "Bercocok Tanam",
      status: true,
    },
    {
      name: "Olahraga",
      status: true,
    },
  ]
  


  const removekey = async (e) => {
    if (e === "checked") {
      console.log(arr, "checked")
      const NewTodos = await arr.filter((item, i) => item["checkbox"] !== true)
      console.log(NewTodos)
      setArr(NewTodos)
    } else {
      const NewTodos  = await arr.filter((item) => item !== arr[e])
      setArr(NewTodos)
    }
    
  }

  const updateInput = (e) => {
    const change = [...arr]
    change[e]['checkbox'] = !change[e]["checkbox"]
    setArr(change)
    console.log(change)
  }

  const handleChange = (e, x= null) => {
    const { name, value } = e.target
    setTodo(value)
  }

  useEffect(() => {
    if (arr.length <= 0) {
      console.log(JSON.stringify(dataJson), dataJson, "data")
    setArr(JSON.parse(JSON.stringify(dataJson)))
    }
    
  }, [])
  
  return (
  
    
    <div>

        <div className={styles.body}>
        <div className={styles.parent} contenteditable>
        <div className={styles.child} contenteditable>
          
        <button onClick={() => removekey("checked")}>Delete</button><br></br>
              <input 
              name="todoText"
              onChange={(e) =>{
                handleChange(e)
              }
              }
              placeholder="Todo Text"
              value={todo}
              type="text"/>
       
        <button
        >add</button>
        <h3 className={styles.h3}>What do you Want?</h3>
            {Object.keys(arr).map((keyName, i) => (
              <form className={styles.form}>
            <FormGroup check>
            <Label check>
            <Input
            key={arr[keyName]["name"] + "_" + i}
            onClick={(e) => updateInput(i)}
            defaultChecked={arr[keyName]["checkbox"]}
            type="checkbox" /> 
            <p className={styles.p}>{arr[keyName]["name"]}</p> 
             </Label>
              <button onClick={() => removekey(i)}>
                remove
              </button>
              </FormGroup>
          </form>
          ))}
        </div>
        </div>
        </div>
    </div>
  )
}    
    



export default Phase1;

//     class Task2 extends React.Component {
//     render(){
//         return(    
  //       <div className={styles.body}>
  //       <div className={styles.parent} contenteditable>
        
  //       <div className={styles.child} contenteditable>
  //       <h3 className={styles.h3}>What do you Want?</h3>
  //               <form className={styles.form}>
  //           <FormGroup check>
  //           <Label check>
  //           <Input type="checkbox" /> 
  //           <p className={styles.p}>Finish Homework</p>
  //            </Label>
  //             </FormGroup>
  //       <FormGroup check>
  //       <Label check>
  //       <Input type="checkbox" /> 
  //       <p className={styles.p}>Cooking</p>
  //       </Label>
  //     </FormGroup>
  //     <FormGroup check>
  //           <Label check>
  //           <Input type="checkbox" /> 
  //          <p className={styles.p}>Praying</p>
  //           </Label>
  //           </FormGroup> 
  //           <FormGroup check>
  //           <Label check>
  //           <Input type="checkbox" /> 
  //          <p className={styles.p}>Sleeping</p>
  //           </Label>
  //           </FormGroup>  
  //         </form>
  // </div>
  
  // </div>
  // </div>
//   );
// //  };  

// }
// export default Task2