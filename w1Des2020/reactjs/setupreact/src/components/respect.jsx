import styles from "../assest/respect.module.css";
import React from "react";

class respect extends React.Component {
    render() { 
        return (
        <div className ={styles.body}>
            <div className={styles.card}>
            <h1 className={styles.h1}>Title Here</h1>
            <div className={styles.visual}></div>
            <p> Descriptive Text. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sed est error repellat veritatis.</p>
            </div>
        </div>
        )
    }   
}

export default respect;