import React from 'react';
import syohan0 from "../Syohan/assest/syohan0.JPG";
import syohan2 from "../Syohan/assest/syohan2.JPG";


const Bio = () => {
    return (
        <div className="Bioname">
            <nav class="navbar navbar-expand-lg">
            <div class="container">

                <a class="navbar-brand" href="#">
                    Syohan's Resume
                </a>
                 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item">
                             <a href="#intro" class="nav-link smoothScroll">Introduction</a>
                        </li>
                        <li class="nav-item">
                            <a href="#about" class="nav-link smoothScroll">About Me</a>
                        </li>
                        <li class="nav-item">
                            <a href="/blog" class="nav-link smoothScroll">Reviews</a>
                        </li>
                        <li class="nav-item">
                            <a href="#contact" class="nav-link smoothScroll">Contact</a>
                        </li>
                    </ul>
                        <div class="mt-lg-0 mt-3 mb-4 mb-lg-0">
                            <a href="#" class="custom-btn btn" download>Download CV</a>
                        </div>
                </div>

            

        </div>
    </nav>


    
    <section className="hero d-flex flex-column justify-content-center align-items-center" id="intro">

         <div class="container">
            <div class="row">

                  <div class="mx-auto col-lg-5 col-md-5 col-10">
                      <img src={syohan0} class="img-fluid"/>
                  </div>

                   <div class="d-flex flex-column justify-content-center align-items-center col-lg-7 col-md-7 col-12">
                        <div class="hero-text">

                            <h1 class="hero-title">👋  Hi Its Me</h1>

                            <a href="#" class="email-link">
                                syoh4ndp@gmail.com
                            </a>
                          
                        </div>
                    </div>

            </div>
        </div>
    </section>


    <section class="about section-padding" id="about">
        <div class="container">
            <div class="row">

                <div class="col-lg-6 col-md-6 col-12">
                    <h3 class="mb-4">This is Syohan's Resume</h3>

                    <p>My Name is Syohan , I come from Jakarta Indonesia</p>

                    <p>Im a Banker & Jr Front End Developer</p>

                    <ul class="mt-4 mb-5 mb-lg-0 profile-list list-unstyled">
                        <li><strong>Full Name :</strong> Syohan Demega Perdhana </li>

                        <li><strong>Date of Birth:</strong> 21 April 1989</li>

                        <li><strong>Region :</strong> Indonesia</li>

                        <li><strong>Email :</strong> syoh4ndp@gmail.com</li>
                    </ul>
                </div>

                    <div className="col-lg-5 mx-auto col-md-6 col-12">
                        <img src={syohan2} class="about-image img-fluid"/>
                    </div>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
            </div>
            <div class="row about-third">
            	<div class="col-lg-4 col-md-4 col-12">
                <h3>JavaScript</h3>
                <p>Sed eu risus tincidunt, finibus dolor non, gravida ex. Donec lacinia mi nec erat tempus, vel consectetur ante scelerisque. Ut blandit, risus in venenatis ultricies, lacus tellus fermentum.</p>
                </div>
                <div class="col-lg-4 col-md-4 col-12">
                <h3>HTML & CSS</h3>
                <p>Cras et nisl vestibulum, accumsan elit sed, pretium enim. Vestibulum in condimentum magna. Maecenas quam magna, iaculis eu turpis et, commodo pulvinar leo.</p>
                </div>
                <div class="col-lg-4 col-md-4 col-12">
                <h3>Banking</h3>
                <p>Duis at mollis leo, venenatis congue ex. Cras urna dui, gravida euismod lectus et, cursus tempor nulla. Praesent at turpis quis ex tristique gravida quis eget eros.</p>
                </div>
            </div>
        </div>
    </section>
                          
    <span class="faq-info-text e-flex">Please send me a message if you have anything to say. Send an email message to </span>

     <section class="contact section-padding pt-0" id="contact"/>
        <div class="container">
            <div class="row">
            <div class="col-lg-6 col-md-6 col-12">
            <form action="#" method="get" class="contact-form webform"  role="form">
                <div class="form-group d-flex flex-column-reverse">
                    <input type="text" class="form-control" name="cf-name" id="cf-name" placeholder="Your Name"/>
                    <label for="cf-name" class="webform-label">Full Name</label>
                </div>
                <div class="form-group d-flex flex-column-reverse">
                    <input type="email" class="form-control" name="cf-email" id="cf-email" placeholder="Your Email"/>
                    <label for="cf-email" class="webform-label">Your Email</label>
                </div>
                <div class="form-group d-flex flex-column-reverse">
                    <textarea class="form-control" rows="5" name="cf-message" id="cf-message" placeholder="Your Message"></textarea>
                    <label for="cf-message" class="webform-label">Message</label>
                </div>
                <button type="submit" class="form-control" id="submit-button" name="submit">Send</button>
            </form>
            </div>
            </div>

            <div class="mx-auto col-lg-4 col-md-6 col-12">
                <h3 class="my-4 pt-4 pt-lg-0">Contact Me</h3>
                <p class="mb-1">+62-878-7700-0412</p>
                <p>
                    <a href="#">
                        syoh4ndp@gmail.com
                    <i class="fas fa-arrow-right custom-icon"></i>
                    </a>
                </p>
            </div>
        

        </div>
      </div>
    


        


    );
}

export default Bio;