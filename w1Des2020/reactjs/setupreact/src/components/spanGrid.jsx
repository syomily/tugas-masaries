import Style from "../assest/SpanGrid.module.css";
import React from "react";

class SpanGrid extends React.Component {
    render() { 
        return (
        <div className ={Style.body}>
            <div className ={Style.span12}>Span 12</div>
            <div className ={Style.span6}>Span 6</div>
            <div className ={Style.span4}>Span 4</div>
            <div className ={Style.span2}>Span 2</div>
        </div>
        )
    }   
}

export default SpanGrid;