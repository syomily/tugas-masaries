import Style from "../assest/pancake.module.css"
import React from "react";

class Pancake extends React.Component {
    render() {
        return <div className={Style.body}>
            <div className={Style.parent}>
                <div className={Style.child}>1</div>
                <div className={Style.child}>2</div>
                <div className={Style.child}>3</div>
            </div>
       </div>
    
    }
    
}
export default Pancake;